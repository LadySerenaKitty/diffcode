#include "diffcode.h"
#include <iomanip>

unsigned char encode_work(unsigned char a, unsigned char b) {
	if (a < b) { return b - a; }
	else if (a > b) { return 0xff - (a - b); }
	return 0;
}

#ifdef __cplusplus
extern "C" {
#endif

void diff_encode(std::istream &is, std::ostream &os) {
	os << std::setfill('0');
	unsigned char prev = 0;
	unsigned char chr = 0;
	while(is.good()) {
		chr = is.get();
		if (is.fail()) { return; }
		os << encode_work(prev, chr);
		prev = chr;
	}
}

void diff_decode(std::istream &is, std::ostream &os) {
	unsigned char prev = 0;
	unsigned char chr = 0;
	unsigned char pt = 0;
	while (is.good()) {
		chr = is.get();
		if (is.fail()) { return; }
		pt = (unsigned char)((prev + chr) & 0xff);
		os << pt;
		prev = pt;
	}
}

#ifdef __cplusplus
}
#endif

