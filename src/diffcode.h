#include <iostream>
#include <string>

#ifdef __cplusplus
extern "C" {
#endif

void diff_encode(std::istream &is, std::ostream &os);
void diff_decode(std::istream &is, std::ostream &os);

#ifdef __cplusplus
}
#endif

