#include "diffcode.h"
#include <fstream>

int main(int argc, char** argv) {

	if (argc <= 1) { diff_decode(std::cin, std::cout); }
	else {
		std::ifstream is(argv[1], std::ios::binary | std::ios::in);
		diff_decode(is, std::cout);
		is.close();
	}

	return 0;
}

