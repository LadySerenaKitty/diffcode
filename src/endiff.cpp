#include "diffcode.h"
#include <fstream>

int main(int argc, char** argv) {

	if (argc <= 1) { diff_encode(std::cin, std::cout); }
	else {
		std::ifstream is(argv[1], std::ios::binary | std::ios::in);
		diff_encode(is, std::cout);
		is.close();
	}

	return 0;
}

