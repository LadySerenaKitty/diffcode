#!/bin/sh

if [ -x build ]; then
	echo Running \"make clean\"
	make clean
fi

CMAKEFILES=`find * -type d -name CMakeFiles`
CMAKECACHE=`find * -type f -name CMakeCache.txt`
MAKEFILES=`find * -type f -name Makefile`
DOXYFILE=`find * -type f -name Doxyfile`
INSTALL=`find * -type f -name cmake_install.cmake`
PKGLIST=`find * -type f -name install_manifest.txt`
BUILDS=`find * -type d -name build`
CORES=`find * -type f -name "*.core"`
VIMS=`find * -type f -name "*~"`

FILES="compile_commands.json"

if [ -n "${CMAKEFILES}" ]; then
	echo Removing CMakeFiles
	rm -rf ${CMAKEFILES}
fi

if [ -n "${CMAKECACHE}" ]; then
	echo Removing CMakeCache
	rm -f ${CMAKECACHE}
fi

if [ -n "${MAKEFILES}" ]; then
	echo Removing Makefiles
	rm -f ${MAKEFILES}
fi

if [ -n "${DOXYFILE}" ]; then
	echo Removing Doxyfiles
	rm -f ${DOXYFILE}
fi

if [ -n "${INSTALL}" ]; then
	echo Removing cmake_install.cmake
	rm -f ${INSTALL}
fi

if [ -n "${PKGLIST}" ]; then
	echo Removing install manifsts
	rm -f ${PKGLIST}
fi

if [ -n "${BUILDS}" ]; then
	echo Removing artifacts
	rm -rf ${BUILDS}
fi

if [ -n "${CORES}" ]; then
	echo Removing core dumps
	rm -f ${CORES}
fi

if [ -n "${VIMS}" ]; then
	echo Removing vim backups
	rm -f ${VIMS}
fi

for F in ${FILES}
do
	if [ -f $F ]; then
		rm -f $F
	fi
done

